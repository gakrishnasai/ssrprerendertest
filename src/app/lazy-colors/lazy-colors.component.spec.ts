import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyColorsComponent } from './lazy-colors.component';

describe('LazyColorsComponent', () => {
  let component: LazyColorsComponent;
  let fixture: ComponentFixture<LazyColorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LazyColorsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LazyColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
