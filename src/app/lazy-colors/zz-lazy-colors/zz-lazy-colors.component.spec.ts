import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZzLazyColorsComponent } from './zz-lazy-colors.component';

describe('ZzLazyColorsComponent', () => {
  let component: ZzLazyColorsComponent;
  let fixture: ComponentFixture<ZzLazyColorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZzLazyColorsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZzLazyColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
