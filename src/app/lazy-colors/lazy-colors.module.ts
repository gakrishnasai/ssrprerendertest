import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyColorsRoutingModule } from './lazy-colors-routing.module';
import { LazyColorsComponent } from './lazy-colors.component';
import { BlueComponent } from './blue/blue.component';
import { YellowComponent } from './yellow/yellow.component';
import { ZzLazyColorsComponent } from './zz-lazy-colors/zz-lazy-colors.component';


@NgModule({
  declarations: [
    LazyColorsComponent,
    BlueComponent,
    YellowComponent,
    ZzLazyColorsComponent
  ],
  imports: [
    CommonModule,
    LazyColorsRoutingModule
  ]
})
export class LazyColorsModule { }
