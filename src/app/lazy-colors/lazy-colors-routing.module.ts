import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlueComponent } from './blue/blue.component';
import { LazyColorsComponent } from './lazy-colors.component';
import { YellowComponent } from './yellow/yellow.component';
import { ZzLazyColorsComponent } from './zz-lazy-colors/zz-lazy-colors.component';

const routes: Routes = [
  {
    path: '', component: LazyColorsComponent,
    children: [
      { path: 'blue', component: BlueComponent },
      { path: 'yellow', component: YellowComponent },
      { path: '', component: ZzLazyColorsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyColorsRoutingModule { }
