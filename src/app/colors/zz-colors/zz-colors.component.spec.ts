import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZzColorsComponent } from './zz-colors.component';

describe('ZzColorsComponent', () => {
  let component: ZzColorsComponent;
  let fixture: ComponentFixture<ZzColorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZzColorsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZzColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
