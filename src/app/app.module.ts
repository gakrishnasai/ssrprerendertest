import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ColorsComponent } from './colors/colors.component';
import { RedComponent } from './colors/red/red.component';
import { GreenComponent } from './colors/green/green.component';
import { HomeComponent } from './home/home.component';
import { ZzColorsComponent } from './colors/zz-colors/zz-colors.component';

@NgModule({
  declarations: [
    AppComponent,
    ColorsComponent,
    RedComponent,
    GreenComponent,
    HomeComponent,
    ZzColorsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
