import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ColorsComponent } from './colors/colors.component';
import { GreenComponent } from './colors/green/green.component';
import { RedComponent } from './colors/red/red.component';
import { ZzColorsComponent } from './colors/zz-colors/zz-colors.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '', component: ColorsComponent,
    children: [
      { path: 'colors', component: ZzColorsComponent },
      { path: 'green', component: GreenComponent },
      { path: 'red', component: RedComponent },
    ]
  },
  { path: 'lazy-colors', loadChildren: () => import('./lazy-colors/lazy-colors.module').then(m => m.LazyColorsModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
